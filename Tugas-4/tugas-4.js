//soal 1
console.log("SOAL 1")

var flag =0
console.log("LOOPING PERTAMA")

while(flag<=18){
    flag+=2
    console.log(flag + " I love coding") 
}

console.log("LOOPING KEDUA")
while(flag>=2){
    console.log(flag + " I will become a frontend developer")
    flag-=2
}

//soal 2
console.log(" ")
console.log("SOAL 2")

for(i=1; i<=20;i++){
    if((i%2!=0)&&(i%3==0)){
        console.log(i+" - I Love Coding")
    }else if(i%2==0){
        console.log(i+" - Berkualitas")
    }else console.log(i+" - Santai")
}


//soal 3
console.log(" ")
console.log("SOAL 3")

for(var result = "#" ; result.length <=7 ; result= result + "#"){
   console.log(result)
}

//soal 4
console.log(" ")
console.log("Soal 4")

var kalimat="saya sangat senang belajar javascript"
var kata=kalimat.split(" ")
console.log(kata)

//soal 5
console.log(" ")
console.log("SOAL 5")

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort()

for(var i=0;i<5;i++){
console.log(daftarBuah[i])
}
