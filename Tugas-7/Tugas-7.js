//SOAL 1
//Release 0
class Animal {
    constructor(name,legs,cold_blooded){
        this.name = name
        this.legs = 4
        this.cold_blooded = false
        
    }
}
var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//Release 1
class Frog extends Animal {
    constructor(name){

        super(name)
        }
        jump = () =>{
            return console.log("Hop Hop")
    }
}

var kodok = new Frog("buduk")
kodok.jump()

console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)


class Ape extends Animal {
    constructor(name,legs){

        super(name)
        this.legs = 2
        }
        yell = () =>{
            return console.log("Auooo")
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell()

console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)


//SOAL 2
class Clock {
  constructor({ template }){
    this.timer;
    this.template=template
  }
  

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(timer);
  };

  start() {
    this.render();
    this.timer = setInterval(this.render.bind(this), 1000);
  };

}

var clock = new Clock({template: 'h:m:s'});
clock.start();