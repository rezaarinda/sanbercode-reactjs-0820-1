//SOAL 1
let luas = (r) => {
    return 22/7*r*r
}
console.log(luas(7))

const keliling = (r) =>{
    return 2*22/7*r
}
console.log(keliling(7))

//SOAL 2
console.log(" ")
let kalimat = ""
const tambahKata = (k1,k2,k3,k4,k5) => {
    kalimat = `${k1} ${k2} ${k3} ${k4} ${k5}`
}
tambahKata("saya","adalah","seorang","frontend","developer")
console.log(kalimat)

//SOAL 3
console.log(" ")
const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(`${firstName} ${lastName}`)
        return 
      }
    }
  }
   
//Driver Code 
newFunction("William", "Imoh").fullName()

//SOAL 4
console.log(" ")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  const {firstName,lastName,destination,occupation,spell} = newObject


console.log(firstName, lastName, destination, occupation)

//SOAL 5
console.log(" ")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]

console.log(combined)

console.log(" ")
